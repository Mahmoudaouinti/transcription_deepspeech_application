//{{NO_DEPENDENCIES}}
// fichier Include Microsoft Visual C++.
// Utilis� par TranscriberModelVF.rc
//
#define IDD_TRANSCRIBERMODELVF_DIALOG   102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     130
#define IDD_DIALOG2                     132
#define IDD_DIALOG3                     134
#define IDC_BUTTON1                     1000
#define IDC_BUTTON2                     1001
#define IDC_BUTTON3                     1002
#define IDC_EDIT1                       1004
#define IDC_EDIT2                       1005
#define IDC_LoadModel                   1006
#define IDC_PROGRESS1                   1007
#define IDC_BrowseWav                   1008
#define IDC_TRANSCRIBE                  1009
#define ID_Clear                        1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
