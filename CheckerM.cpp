// CheckerM.cpp : fichier d'implémentation
//

#include "pch.h"
#include "TranscriberModelVF.h"
#include "CheckerM.h"
#include "afxdialogex.h"


// boîte de dialogue de CheckerM

IMPLEMENT_DYNAMIC(CheckerM, CDialogEx)

CheckerM::CheckerM(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG3, pParent)
{

}


void CheckerM::set_STT(Speech_To_Text* STT)
{
	stt = STT;
}


CheckerM::~CheckerM()
{
}

void CheckerM::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CheckerM, CDialogEx)
	ON_WM_TIMER()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// gestionnaires de messages de CheckerM


void CheckerM::OnTimer(UINT_PTR nIDEvent)
{
	if ((i < 80) && !stt->isLoaded()) {
		i += 10;
		myCtrl->SetPos(i);
	}
	if (stt->isLoaded()) {
		myCtrl->SetPos(98);
		this->EndDialog(true);
	}

	//CDialogEx::OnTimer(nIDEvent);
}


void CheckerM::OnShowWindow(BOOL bShow, UINT nStatus)
{
	//CDialogEx::OnShowWindow(bShow, nStatus);

	// TODO: ajoutez ici le code de votre gestionnaire de messages

	myCtrl = (CProgressCtrl*)GetDlgItem(IDC_PROGRESS1);
	SetTimer(1, 4000, NULL);
	myCtrl->SetRange(0, 100);
	myCtrl->SetPos(0);



}
