
// TranscriberModelVFDlg.cpp : fichier d'implémentation
//

#include "pch.h"
#include "stdafx.h"
#include "TranscriberModelVF.h"
#include "TranscriberModelVFDlg.h"
#include "afxdialogex.h"
#include "ModelLoadingArea.h"
#include "Transcription.h"
#include "CheckerM.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// boîte de dialogue de CTranscriberModelVFDlg



CTranscriberModelVFDlg::CTranscriberModelVFDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TRANSCRIBERMODELVF_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTranscriberModelVFDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTranscriberModelVFDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CTranscriberModelVFDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CTranscriberModelVFDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// gestionnaires de messages de CTranscriberModelVFDlg

BOOL CTranscriberModelVFDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	stt = new Speech_To_Text();
	

	



	// Définir l'icône de cette boîte de dialogue.  L'infrastructure effectue cela automatiquement
	//  lorsque la fenêtre principale de l'application n'est pas une boîte de dialogue
	SetIcon(m_hIcon, TRUE);			// Définir une grande icône
	SetIcon(m_hIcon, FALSE);		// Définir une petite icône

	// TODO: ajoutez ici une initialisation supplémentaire

	return TRUE;  // retourne TRUE, sauf si vous avez défini le focus sur un contrôle
}

// Si vous ajoutez un bouton Réduire à votre boîte de dialogue, vous devez utiliser le code ci-dessous
//  pour dessiner l'icône.  Pour les applications MFC utilisant le modèle Document/Vue,
//  cela est fait automatiquement par l'infrastructure.

void CTranscriberModelVFDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // contexte de périphérique pour la peinture

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrer l'icône dans le rectangle client
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dessiner l'icône
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Le système appelle cette fonction pour obtenir le curseur à afficher lorsque l'utilisateur fait glisser
//  la fenêtre réduite.
HCURSOR CTranscriberModelVFDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

 bool dirExists(const std::string& dirName_in)
 {
	 DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	 if (ftyp == INVALID_FILE_ATTRIBUTES)
		 return false;  //something is wrong with your path!

	 if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		 return true;   // this is a directory!

	 return false;    // this is not a directory!
 }

 inline bool exists_test3(const std::string& name) {
	 struct stat buffer;
	 return (stat(name.c_str(), &buffer) == 0);
 }

 bool model_files_not_exist(std::string language)

{
	 std::string current1 = fs::current_path().string();
	 std::replace(current1.begin(), current1.end(), '\\', '/');
	
	 if (!dirExists(current1 + "\\models"))
	{
		 std::string res1 = current1 + "\\models";
		CreateDirectoryA(res1.c_str(), NULL);
		
	}
	
	std::string folder2 = current1 + "\\models\\" + language;
	if (!dirExists(folder2)) {
		
		CreateDirectoryA(folder2.c_str(), NULL);
	}

	if (
		
		exists_test3(current1 + "\\models\\" + language + "\\trie") &&
		exists_test3(current1 + "\\models\\" + language + "\\output_graph.pbmm") &&
		exists_test3(current1 + "\\models\\" + language + "\\lm.binary"))
	{
		return true;
		}
	else
	{

		return false;
	}


	return false;

}
 void CTranscriberModelVFDlg::loadModal(const char* lang) {
	if (!model_files_not_exist(lang)) 
	{

		ShowWindow(SW_HIDE);
		ModelLoadingArea* Load = new ModelLoadingArea();
		Load->DoModal();
		ShowWindow(SW_SHOW);
	}
	else {
		
			MessageBox(L"Model Loaded successfuly");
			ShowWindow(SW_HIDE);
			Transcription* transcription = new Transcription;
			transcription->DoModal();
			ShowWindow(SW_SHOW);
		
	}

}

void CTranscriberModelVFDlg::OnBnClickedButton1()
{
	
	type_model = "fr";
	loadModal("fr");
}


void CTranscriberModelVFDlg::OnBnClickedButton2()
{
	type_model = "en";
	loadModal("en");
}
