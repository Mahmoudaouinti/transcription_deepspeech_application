#pragma once


// boîte de dialogue de ModelLoadingArea

class ModelLoadingArea : public CDialogEx
{
	DECLARE_DYNAMIC(ModelLoadingArea)

public:
	ModelLoadingArea(CWnd* pParent = nullptr);   // constructeur standard

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//virtual ~ModelLoadingArea();

// Données de boîte de dialogue
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

	DECLARE_MESSAGE_MAP()
private:
	
	std::vector<std::thread> services;
	std::thread t1;
	CEdit PATH_MODEL;
	CString m_strPathnameModel;
	Speech_To_Text* stt;


public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedLoadmodel();
	std::string ExePath();
	std::string filename;
	~ModelLoadingArea();
	


};
