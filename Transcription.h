#pragma once


// boîte de dialogue de Transcription

class Transcription : public CDialogEx
{
	DECLARE_DYNAMIC(Transcription)

public:

	Transcription(CWnd* pParent = nullptr);   // constructeur standard
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual ~Transcription();

// Données de boîte de dialogue
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG2 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

	DECLARE_MESSAGE_MAP()

private: 

	Speech_To_Text* stt;
	CEdit PATH_WAV;
	CString  m_strPathnameWAV;
	CEdit EditOutput;
	CButton* TRANSCRIBE;
	CButton* Clear;
	std::vector<std::thread> services;
	std::thread t1;

public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBrowsewav();
	afx_msg void OnBnClickedTranscribe();
	
	afx_msg void OnBnClickedClear();
};
