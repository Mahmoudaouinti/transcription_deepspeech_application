#pragma once


// boîte de dialogue de CheckerM

class CheckerM : public CDialogEx
{
	DECLARE_DYNAMIC(CheckerM)

public:
	CheckerM(CWnd* pParent = nullptr);   // constructeur standard
	void set_STT(Speech_To_Text* stt);
	virtual ~CheckerM();


private:
	Speech_To_Text* stt;
	int i = 0;
	CProgressCtrl* myCtrl;
// Données de boîte de dialogue
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG3 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
