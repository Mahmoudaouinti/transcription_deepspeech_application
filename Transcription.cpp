// Transcription.cpp : fichier d'implémentation
//

#include "pch.h"
#include "TranscriberModelVF.h"
#include "Transcription.h"
#include "afxdialogex.h"
#include "TranscriberModelVFDlg.h"


// boîte de dialogue de Transcription

IMPLEMENT_DYNAMIC(Transcription, CDialogEx)

Transcription::Transcription(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG2, pParent)
{
	
	//PATH_WAV.GetDlgItem(IDC_EDIT1)->GetSafeHwnd();
	//PATH_WAV.SetWindowTextW(L"Select WAV File ...");
	//EditOutput = (CEdit*)GetDlgItem(IDC_EDIT2);
	
	stt = new Speech_To_Text();
	stt->initVoice(&EditOutput);
	
}

Transcription::~Transcription()
{
}

void Transcription::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, PATH_WAV);
	PATH_WAV.SetWindowTextW(L"Select WAV File ...");
	DDX_Control(pDX, IDC_EDIT2, EditOutput);
	TRANSCRIBE = (CButton*)GetDlgItem(IDC_TRANSCRIBE);
	Clear = (CButton*)GetDlgItem(ID_Clear);
	//EditOutput = (CEdit*)GetDlgItem(IDC_EDIT2);
}
BOOL Transcription::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_ESCAPE)  return TRUE;
		if (pMsg->wParam == VK_RETURN)  return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(Transcription, CDialogEx)
	ON_BN_CLICKED(IDOK, &Transcription::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BrowseWav, &Transcription::OnBnClickedBrowsewav)
	ON_BN_CLICKED(IDC_TRANSCRIBE, &Transcription::OnBnClickedTranscribe)
	
	ON_BN_CLICKED(ID_Clear, &Transcription::OnBnClickedClear)
END_MESSAGE_MAP()


// gestionnaires de messages de Transcription


void Transcription::OnBnClickedOk()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contrôle
	ShowWindow(SW_HIDE);
	CTranscriberModelVFDlg* pDlg = new CTranscriberModelVFDlg;
	pDlg->DoModal();
	ShowWindow(SW_SHOW);
	CDialogEx::OnOK();
}


void Transcription::OnBnClickedBrowsewav()
{
	CFileDialog fileDlgModel(TRUE, L"WAV", L".wav", OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, L"WAV File (*.WAV)|*.WAV||", this);
	fileDlgModel.DoModal();
	m_strPathnameWAV = fileDlgModel.GetPathName();
	UpdateData(FALSE);
	PATH_WAV.SetWindowTextW(m_strPathnameWAV);
}


void Transcription::OnBnClickedTranscribe()
{
	std::string model1 = "models/" + type_model + "/output_graph.pbmm";
	stt->model = const_cast<char*>(model1.c_str());
	std::string lm1 = "models/" + type_model + "/lm.binary";
	stt->lm = const_cast<char*>(lm1.c_str());
	std::string trie1 = "models/" + type_model + "/trie";
	stt->trie = const_cast<char*>(trie1.c_str());


	CString windowTxt;
	PATH_WAV.GetWindowTextW(windowTxt);
	std::string strp = stt->CStringToString(windowTxt);
	char* char_path;
	char_path = new char[strp.length() + 1];      //verify conversion from string to char*
	strcpy(char_path, strp.c_str());
	stt->loadModel();
	stt->setAudio(char_path);
	TRANSCRIBE->EnableWindow(0);
	//stt->multiProcess(0);
	services.push_back(std::thread(&Speech_To_Text::multiProcess, stt, 0));
}

void Transcription::OnBnClickedClear()
{
	Clear->EnableWindow(TRANSCRIBE->EnableWindow(1));
	
}
