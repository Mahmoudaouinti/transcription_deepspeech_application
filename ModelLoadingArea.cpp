// ModelLoadingArea.cpp : fichier d'implémentation
//

#include "pch.h"
#include "stdafx.h"
#include "TranscriberModelVF.h"
#include "TranscriberModelVFDlg.h"
#include "ModelLoadingArea.h"
#include "afxdialogex.h"
#include "Transcription.h"
#include "CheckerM.h"
std::string type_model ;

// boîte de dialogue de ModelLoadingArea

IMPLEMENT_DYNAMIC(ModelLoadingArea, CDialogEx)

ModelLoadingArea::ModelLoadingArea(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
{
	
	//PATH_MODEL = (CEdit*)GetDlgItem(IDC_EDIT1);
	stt = new Speech_To_Text();
	
}

/*ModelLoadingArea::~ModelLoadingArea()
{
}*/

void ModelLoadingArea::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, PATH_MODEL);
	PATH_MODEL.SetWindowTextW(L"Select Model File ...");
}
BOOL ModelLoadingArea::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_ESCAPE)  return TRUE;
		if (pMsg->wParam == VK_RETURN)  return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(ModelLoadingArea, CDialogEx)
	ON_BN_CLICKED(IDCANCEL, &ModelLoadingArea::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &ModelLoadingArea::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_LoadModel, &ModelLoadingArea::OnBnClickedLoadmodel)
END_MESSAGE_MAP()


// gestionnaires de messages de ModelLoadingArea


void ModelLoadingArea::OnBnClickedCancel()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contrôle
	CDialogEx::OnCancel();
}

ModelLoadingArea::~ModelLoadingArea()
{
	for (auto& th : services) {
		th.join();
	}
}

void ModelLoadingArea::OnBnClickedButton1()
{
	CFileDialog fileDlgModel(TRUE, L"zip", L".zip", OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, L"ZIP File (*.ZIP)|*.ZIP||", this);
	fileDlgModel.DoModal();
	m_strPathnameModel = fileDlgModel.GetPathName();
	UpdateData(FALSE);
	PATH_MODEL.SetWindowTextW(m_strPathnameModel);
}

	
bool Unzip2Folder(BSTR lpZipFile, BSTR lpFolder)
{
	IShellDispatch* pISD;

	Folder* pZippedFile = 0L;
	Folder* pDestination = 0L;

	long FilesCount = 0;
	IDispatch* pItem = 0L;
	FolderItems* pFilesInside = 0L;

	VARIANT Options, OutFolder, InZipFile, Item;
	CoInitialize(NULL);
	__try {
		if (CoCreateInstance(CLSID_Shell, NULL, CLSCTX_INPROC_SERVER, IID_IShellDispatch, (void**)& pISD) != S_OK)
			return 1;

		InZipFile.vt = VT_BSTR;
		InZipFile.bstrVal = lpZipFile;
		pISD->NameSpace(InZipFile, &pZippedFile);
		if (!pZippedFile)
		{
			pISD->Release();
			return 1;
		}

		OutFolder.vt = VT_BSTR;
		OutFolder.bstrVal = lpFolder;
		pISD->NameSpace(OutFolder, &pDestination);
		if (!pDestination)
		{
			pZippedFile->Release();
			pISD->Release();
			return 1;
		}

		pZippedFile->Items(&pFilesInside);
		if (!pFilesInside)
		{
			pDestination->Release();
			pZippedFile->Release();
			pISD->Release();
			return 1;
		}

		pFilesInside->get_Count(&FilesCount);
		if (FilesCount < 1)
		{
			pFilesInside->Release();
			pDestination->Release();
			pZippedFile->Release();
			pISD->Release();
			return 0;
		}

		pFilesInside->QueryInterface(IID_IDispatch, (void**)& pItem);

		Item.vt = VT_DISPATCH;
		Item.pdispVal = pItem;

		Options.vt = VT_I4;
		Options.lVal = 1024 | 512 | 16 | 4;

		bool retval = pDestination->CopyHere(Item, Options) == S_OK;

		pItem->Release(); pItem = 0L;
		pFilesInside->Release(); pFilesInside = 0L;
		pDestination->Release(); pDestination = 0L;
		pZippedFile->Release(); pZippedFile = 0L;
		pISD->Release(); pISD = 0L;

		return retval;

	}
	__finally
	{
		CoUninitialize();
	}
}

std::string ModelLoadingArea::ExePath() {
	char buffer[MAX_PATH];
	GetModuleFileNameA(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");
	return std::string(buffer).substr(0, pos);
}

std::string getFileName(std::string filePath, bool withExtension = true)
{
	// Create a Path object from File Path
	fs::path pathObj(filePath);

	// Check if file name is required without extension
	if (withExtension == false)
	{
		// Check if file has stem i.e. filename without extension
		if (pathObj.has_stem())
		{
			// return the stem (file name without extension) from path object
			return pathObj.stem().string();
		}
		return "";
	}
	else
	{
		// return the file name with extension from path object
		return pathObj.filename().string();
	}

}

std::string bstr_to_str(BSTR source) {
	//source = L"lol2inside";
	_bstr_t wrapped_bstr = _bstr_t(source);
	int length = wrapped_bstr.length();
	char* char_array = new char[length];
	strcpy_s(char_array, length + 1, wrapped_bstr);
	return char_array;
}

void ModelLoadingArea::OnBnClickedLoadmodel()
{

	CheckerM* check = new CheckerM;
	check->set_STT(stt);
	std::thread t1(&CheckerM::DoModal, check);
	//type_model = "";

	CString windowTxt_pathModel ;
	PATH_MODEL.GetWindowTextW( windowTxt_pathModel);
	USES_CONVERSION;
	LPTSTR lpszStr2 = windowTxt_pathModel.GetBuffer(windowTxt_pathModel.GetLength() + 1);

	std::string current = fs::current_path().string();
	
	CComBSTR file(lpszStr2);
	std::string filestring = bstr_to_str(file);
	std::string filename = getFileName(filestring);
	filename.erase(std::remove(filename.begin(), filename.end(), '.zip'), filename.end());
	std::string res = current + "\\models\\" + type_model;
	CComBSTR folder(res.c_str());
	Unzip2Folder(file, folder);

	//-----------------------------------------
	
	std::this_thread::sleep_for(4s);
	
	check->EndDialog(1);
	
	stt->loadModel();

	t1.join();

	
	
		MessageBox(L"Model Loaded successfuly");
		ShowWindow(SW_HIDE);
		Transcription* transcription = new Transcription;
		transcription->DoModal();
		ShowWindow(SW_SHOW);

	
		
}
