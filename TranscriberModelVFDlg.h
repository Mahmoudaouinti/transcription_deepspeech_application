
// TranscriberModelVFDlg.h : fichier d'en-tête
//

#pragma once


// boîte de dialogue de CTranscriberModelVFDlg
class CTranscriberModelVFDlg : public CDialogEx
{
// Construction
public:
	Speech_To_Text* stt;
	
	CTranscriberModelVFDlg(CWnd* pParent = nullptr);	// constructeur standard

// Données de boîte de dialogue
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TRANSCRIBERMODELVF_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Prise en charge de DDX/DDV


// Implémentation
protected:
	HICON m_hIcon;

	// Fonctions générées de la table des messages
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	void loadModal(const char* lang);

	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};
